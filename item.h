/* KO
// Copyright (C) 2017 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ITEM_H
#define ITEM_H

#include <Urho3D/Urho3D.h>
#include "luckey.h"

class Item : public Object
{
    URHO3D_OBJECT(Item, Object);
public:
    Item(Context* context);
private:
    IntVector2 size_;
    float weight_;
//    Texture2D image_;
};

#endif // ITEM_H
