![Greyface poster](Docs/BIP.png)
# [:four_leaf_clover:](http://www.luckeyproductions.nl/) KO and the Curse of Greyface

[![Build Status](https://travis-ci.org/LucKeyProductions/KO.svg?branch=master)](https://travis-ci.org/LucKeyProductions/KO)

### Summary
Storm the Black Iron Prison, trip your balls inside-out and lift the Curse of Greyface.

![OR Banner](Docs/ORBanner.png)![Greyface poster](Docs/GreyfacePoster.png)![OR Banner](Docs/ORBanner.png)

### Controls
Dual analog game controller or WASD.

<!--
#### Compiling from source

You may try compiling by running this line in a terminal:

```
git clone https://github.com/LucKeyProductions/KO; cd KO; ./install.sh; cd ..; rm -rf KO
```
 -->
### Screenshot
![KO facing five eyes](Screenshots/Screenshot_Mon_Jun__6_21_23_32_2016.png)

### Tools
- [Urho3D](http://urho3d.github.io)
- [QtCreator](http://wiki.qt.io/Category:Tools::QtCreator)
- [Blender](http://www.blender.org/)
- [Inkscape](http://inkscape.org/)
- [GIMP](http://gimp.org)
- [SuperCollider](http://supercollider.github.io/)
- [Audacity](http://web.audacityteam.org/)
