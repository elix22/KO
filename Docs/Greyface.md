# The story of Greyface

Greyface is a smart dinosaur with a growth hormone deficiency. His true name is Crevice, son of Chaos. He is the result of the Controller's first attempt at creating a messiah and the reason he later sent an asteroid to Earth. (If Jesus were Data, this guy is Lore)

Instead of bringing peace between dinosaurs Crevice misused the knowledge he was given to make as many creatures as possible serve him. With the Earth corrupted and deemed lost its creator decided to push the button, assuming Crevice would perish. But he did not. He survived and has held a vendetta against Chaos ever since. As one of the few surviving dinosaurs Crevice soon found other species to do his bidding. 

For millennia he ruled over a kingdom of rodents, eventually betraying his most loyal servants; the rats. He betrayed them to gain trust and loyalty from cats. His rule over cats came to hang in the balance as these creatures value appearances, and so he had to dress up in rat fur to maintain his position as supreme ruler of Earth. With it he also changed his name to Greyface.

Then humans came along and through the creation of toxoplasmosis they were bound to Greyface's domain, and so human civilisation began. Cats were provided with food and housing and all of a sudden Greyface had hands at his disposal that he controlled from the shadows that could develop quite interesting technology. Maybe this could help him develop a weapon with which he could take revenge on Chaos. Greyface was getting tired of creating new religions to piss off his dad and carefully introduced science to humanity.

This of course created the internet after which the true power structures of the world could no longer remain hidden. Humanity had been killing itself off for the leisure of cats. Most people felt so stupid they denied it was real by watching rewinds of TV shows from the time nobody knew this (partial) truth: While we thought man was calling the shots all along we lived under pussy rule.

Now KO, the third son of Chaos, awakens. Will he set things straight and make the world enjoyable once again?

![Greyface poster](GreyfacePoster.png)

Some features:

+ Golden eagles nest
+ Sphinx cat harem
+ Talks like a lolcat (propaganda): haz, iz, cuz, ur/yer, wif, werd
+ Bribes people with fur coats made from disloyal felines
+ Theme song inspired by Roni Size - Mad Cat
