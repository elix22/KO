# Ideas for KO

KO Playstyle sources:

- [O-Line](https://en.wikipedia.org/wiki/American_football_positions#Offensive_\(interior\)_line)

Many mythical or otherwise cultural references including mythical versions of real people.

## Technical additions

- Pathfinding
- Line of Sight
- Books in markdown
- Switch to EMP map format once that is usable.
 
 
### Menu

A book with a cover that changes with the active option. Escape turns the book showing the back prompting you to confirm to exit along with some credits and barkcode.
This same book is carried at all times and holds maps and your notes which are written and erased as you type. Neatness of script depends on tempo of typing hyphens are used at line-endings, erasing does _not_ move text after that. When the gamepad is used a twin-circle keyboard appears.

### Death

When you die you enter a dreamy park environment called _reality_. 魔大农 sits on a bench, sitting down will make him ask you: "Are you ready to enjoy your freedom?"

- "No, I _have_ to save the world!" -> "Good luck then!"
- "Give me liberty or give me death!" -> "OK, have fun!"

As you "respawn" and leave your bed a fellow resident tells you it's good to see that you're feeling better and describes your condition (referencing David Firth's The Unfixable Thought Machine).
Instead of sitting down you can also put money in 魔大农's hat which will make him thank you, race off and return with one of several items that he will consume and offer to share with you.
Musical inspiration: Nina Soul - Sleeping Trees

## World
 
### Story and Environment

#### Intro

In a Mario Bros.-like dream that doesn't scroll and a menu titled Sir/Lady Kris O'Line (Christ Online / Crystalline) there is only one way to progress and leave the dream: Pick up the mushroom to wake up and leave the 2(.3)Droom.
There's a suspicious mailbox at the side of the path to the birdhouse that nobody remembers putting there. It turns out to be a shapeshifter carrying a letter.

#### Present
The world became this way by means of controlled direct democracy through TeleFace.

#### The City

This is the most populous - and only generally known - settlement that remains of human existence. Most of the Earth is being automined by bots after most places were bombed to dunes because people there started opposing the increasingly dictatorial Order of Riches. People living under it are demoralized and lied to, most of them accept their servitude with pride even. Among other things, the OR controls the water and power supply.

#### The Last Tree

![Birdhouse](Birdhouse.png)

The existence of this plant hangs in the balance. The thing that has kept it standing is a community of eight people living inside a house that the tree grew under. But now an order came from the BIP for eviction of the tree house. Which is where our story begins.

Few people know about the opening in side of the tree where you can enter its magical inner chamber.

#### Fake Forest

Fake Forest is what surrounds most of the city, the power trees they consist of are semi-tree-shaped solar panel arrays. Everything is very brightly colored and plastic. Cyborg dogs keep it "safe".

#### Black Iron Prison

At the end of Mainstreet lies number 101, a giant pyramid covered in black cast iron slabs and glowing circuitry, a golden top and cross-barred square windows. KO goes here to file a complaint and kick bubblegum. Upward staircases lead to higher floors decreasing in area, but increasing in complexity and difficulty.

This is the main dungeon for The Curse of Greyface.
Ways in:
 
+ Reception/complaints/helpdesk
+ By train (requires gasmask)

The first floor has a downward ladder in a broom closet that leads to floor 0, a big room with deserted shops and a collapsed downward staircase. There's a blubbering idiot seemingly aimlessly walking around.  

##### Themes

+ Office
+ Factory
+ Slaughterhouse
+ Biolab
+ Satanic church
+ Skynet
+ Party

#### Colt-Rouen Château

A semi-castle occupied by ghosts.

#### Prologue

The residents of the Birdhouse receive a letter from the OR. Whether or not they comply, in line with OR policy in 23 days the tree will be nuked for being abnormal.

You becomes really angry and set off to have a chat with the source of this unacceptable demand.

 
### Playable characters

 Character | Default weapon | Magic focus | Main skillset 
-----------|----------------|-------------|-------------
  __KO__   | Sword & Shield | Life        | Focus
 __Dizzy__ | Poi Blades     | Hot         | Tinkering

#### Magic schools

> Rock, paper scissors?  
> Water douses flames, flames burn plant, plant drinks water

Each magic school is associated with one damage type; heat, cold and poison.

##### Hot magic (Red)

Hot magic includes fire and lightning spells. Flames can ignite flammable equipment/items/objects/characters and thaw things that are frozen. Lightning deals all its damage as it hits, more when hitting a wet surface or robot.

- Fireball (flaming projectile)
- Discharge (ring of lightning)
- Lava blob (fiery liquid)
- Lightning (jumps)
- Volcano (emits lava + blobs)

##### Cool magic (Blue)

Cool magic has spells that create water, ice or wind. Water and ice douse flames, wind spells intensify fire.

- Sprinkle (douse flames, help plants grow)
- Wind gust (blow things away)
- Ice disc (projectile)
- Frost (freeze water and enemies)
- Ice claws (cooler than Wolverine)
- Ice fork (Y-shaped barricade that deals damage when created)

##### Life magic (Green)

Life magic can heal, grow plants or summon animals.

- Heal other (restore health)
- Vine trap (thorny immobilization, upgrades to poisonous)
- Summon rat (fierce rodent) upgrades to catalry/golierat, requires food
- Fairy ring (plenty of mushrooms)
- Maggot brain (raise corpse)

##### Money magic (Gold)

Money magic uses money instead of mana. It is mainly used by bosses.

- Goons (thug assistance)
- Piano (on your head) 
- Golden cage (immobilizes)

#### Skillsets

##### Focus

- Hidden door/trap/item detection
- Critical strike
- Meditate (restore health/attention)
- Bravery (fear resistance)

##### Tinkering

Often requires tools

- Hack/Disarm
- Repair
- Create
- Salvage/Recycle

##### Sport

- Throw (First rocks later knives)
- Dash
- Budge (Clear passages)

##### Weapons

- Single-handed weapon and shield
- Dual-weild, no shield
- Ranged weapons

#### Actions

- Combat
	+ Button bash combos
	  * Sword: Pierce and hack/slash
	  * Shield: Block and bash
    + Magic
      * Spells: Cast while held or released, depending on the spell
      * Attacks can have hot, cool or life (poison) magic damage added by holding down alt-casting. This takes a short while to fully activate during which it already costs mana. When active the weapon will catch fire, freeze or grow spines depending on the type of magic.
- Jump/hop
    + Can bridge small gaps and evade traps. Also used to hit higher.
- Meditation
	+ Restores health and attention
- Throw
	+ Most items can be thrown, with some it has effect. Like with Molotov Cocktails.
- Kick
   + For interacting with the environment and knockback
   
##### Conversations

During a conversation all characters involved have an emotion indicator that shows their current emotion state (ES) and ESs which may unlock new dialog options, give you items or passage. Picking certain dialog options, offering items or bribes may or may not have an effect on a character's ES.

### NPCs


#### Resident

> We're so lucky to be alive with all these video games around!

#### Kerrence Mantenna

Based on Terrence McKenna. Has good mushrooms.

> "Welcome to Gnomesland"  
> "Language controls our every move"  
> "A shrew mushroom array"  

#### Dirk Elfborg

In between Daniel Ellsberg and Dirk Gently. Knows a thing or two.

> "Stay a while and listen"

#### Super Mark  

Some hero who's in it for the money. Does not sell lemonade.  

> "Good deals, bad deals, you name it!"

#### Packrat

Follows you around, helps carry stuff.

#### Magic seals

There be 7 which allow for a free ride home

### Monsters

- Riot police
- Genetically modified crops (frankenfoods)
- Cydogs
- Gnomeknights
- Zombies (prefer the brainiest)
	+ Native American
	+ Construction worker
	+ Cop
	+ GI
	+ Cowboy
- Visha Kanya (venomous & feminine)
- Hells angels (hawkmen that use fire magic)
- Handy men (walking hand connected to male torso )
- Bull ants (wield grass blades, occasionally drop bullshit)
- Robots (Hackable when stunned)
    + Fleyes (call for reinforcements)
    + Punchbot (getting old)
    + Coffeecrawler (caffeinated claws)
    + Laserbot ('cause its hot)
- Clockwork chests
    + Chicken (runs away)
    + ED-209 (you have 20 seconds to comply)

### Bosses

- Santa
- The Five Eyes (in a room with many screens)
- Mecha Weishaupt (in a room inspired by Neuschwanstein)
- Greyface


### Shrines

Shrines have an area of effect. Each one is accompanied by a sage that is willing to trade when no monsters are near. Some will assist in fighting, others hide.

#### Shrine of Jah

Triples effect of meditating. Bob is it's sage.

#### Shrine of [Gurzil](https://en.wikipedia.org/wiki/Gurzil)

Double damage. Daya is the sage.

#### Shrine of [Serket](https://en.wikipedia.org/wiki/Serket)

Cure poison

#### Shrine of [Aceso](https://en.wikipedia.org/wiki/Aceso)

Heal