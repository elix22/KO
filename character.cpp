/* KO
// Copyright (C) 2015 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

//#include "mastercontrol.h"
#include "kocam.h"

#include "character.h"

void Character::RegisterObject(Context *context)
{
    context->RegisterFactory<Character>();
}

Character::Character(Context *context):
    Controllable(context),
    initialHealth_{1.0f},
    health_{initialHealth_},
    firstHitBy_{0},
    lastHitBy_{0},
    skinMaterial_{MC->GetMaterial("Skin")->Clone()},
    hairMaterial_{MC->GetMaterial("Hair")->Clone()},
    irisMaterial_{MC->GetMaterial("Hair")->Clone()}
{
    actionInterval_[HACK] = 0.23f;
    actionInterval_[CAST] = 1.0f;
    actionInterval_[LAUGH] = Random(23.0f);
}

void Character::DelayedStart(Node *node)
{ if (!node) return;

    Controllable::DelayedStart(node_);

    actionSince_[HACK] = actionInterval_[HACK];
    actionSince_[CAST] = actionInterval_[CAST];
    actionSince_[LAUGH] = 1e-5f;


    for (int s{0}; s < 5; ++s) {

        SoundSource3D* sampleSource{ node_->CreateComponent<SoundSource3D>(LOCAL) };
        sampleSource->SetDistanceAttenuation(5.0f, 23.0f, 2.0f);
        sampleSource->SetGain(0.05f);
        sampleSource->SetSoundType(SOUND_EFFECT);
        sampleSources_.Push(sampleSource);
    }

    if (NETWORK->IsServerRunning()) {

        node_->SetRotation(Quaternion(Random(360.0f), Vector3::UP));
        model_->SetModel(MC->GetModel("Male"));
        model_->SetMaterial(0, skinMaterial_);
        //    model_->SetMaterial(1, MC->GetMaterial("Teeth"));
        //    model_->SetMaterial(2, MC->GetMaterial("Gums"));
        //    model_->SetMaterial(3, MC->GetMaterial("Underpants"));

        model_->SetMaterial(4, skinMaterial_);
        //    model_->SetMaterial(5, MC->GetMaterial("EyeWhite"));
        model_->SetMaterial(6, MC->GetMaterial("Cornea"));
        model_->SetMaterial(7, irisMaterial_);
        model_->SetMaterial(8, MC->GetMaterial("Darkness"));
        model_->SetMaterial(9, hairMaterial_);

        for (int e{0}; e < 4; ++e) {

            AnimatedModel* equipment{ node_->CreateComponent<AnimatedModel>(REPLICATED) };

            switch (e) {
            case 0:
                switch(Random(3)) {
                case 0: default:
                    break;
                case 1:
                    equipment->SetModel(MC->GetModel("HairMohawk"));
                    break;
                case 2:
                    equipment->SetModel(MC->GetModel("HairHorns"));
                    break;
                }

                break;
            case 1:
                equipment->SetModel(MC->GetModel("MaleShirt"));
                break;
            case 2:
                switch(Random(2)) {
                case 0:
                    equipment->SetModel(MC->GetModel("LoosePants"));
                    break;
                case 1:
                    equipment->SetModel(MC->GetModel("Shorts"));
                    break;
                }
                break;
            case 3:
                switch(Random(3)) {
                case 0: default:
                    break;
                case 1:
                    equipment->SetModel(MC->GetModel("Beard"));
                    break;
                case 2:
                    equipment->SetModel(MC->GetModel("Fumanchu"));
                    break;
                }
                break;
            case 4:
                equipment->SetModel(MC->GetModel("Shield"));
                break;
            case 5:
                equipment->SetModel(MC->GetModel("Sword"));
                break;
            }


            Model* model{ equipment->GetModel() };
            if (model) {
                switch(e) {
                case 0: equipment->SetMaterial(hairMaterial_);
                    break;
                case 1: equipment->SetMaterial(MC->GetMaterial("Cloth")->Clone());
                    break;
                case 2: equipment->SetMaterial(MC->GetMaterial("Pants")->Clone());
                    break;
                case 3: equipment->SetMaterial(hairMaterial_);
                    break;
                case 4:
                    equipment->SetMaterial(0, MC->GetMaterial("Metal"));
                    equipment->SetMaterial(1, MC->GetMaterial("Leather"));
                    break;
                case 5: equipment->SetMaterial(MC->GetMaterial("Metal"));
                    break;
                default:
                    break;
                }
                equipment->SetCastShadows(true);
                equipment_[e] = equipment;
            }
        }

        Randomize();

        rightHand_ = node_->GetChild("Hand.R", true)->CreateComponent<StaticModel>();
        rightHand_->SetCastShadows(true);

        leftHand_ = node_->GetChild("Wrist.L", true)->CreateComponent<StaticModel>();
        leftHand_->SetCastShadows(true);


        rigidBody_->SetFriction(0.0f);
        rigidBody_->SetRestitution(0.0f);
        rigidBody_->SetMass(1.0f);
        //    rigidBody_->SetLinearFactor(Vector3::ONE - Vector3::UP);
        rigidBody_->SetLinearDamping(0.9999f);
        rigidBody_->SetAngularFactor(Vector3::UP);
        rigidBody_->SetAngularDamping(1.0f);
        rigidBody_->SetLinearRestThreshold(0.01f);
        rigidBody_->SetAngularRestThreshold(0.1f);

        collisionShape_->SetCapsule(0.3f, 0.5f, Vector3::UP * 0.25f);
    }
//    EquipLeftHand();
//    EquipRightHand();
}

void Character::Randomize()
{
//    node_->SetScale(Vector3(1.0f, Random(0.95f, 1.1f), 1.0f));

    //Randomize colors
    Color skinColor{ LucKey::RandomSkinColor() };
    skinMaterial_->SetShaderParameter("MatDiffColor", skinColor);

    Color hairColor{ LucKey::RandomHairColor(skinColor, true) };
    hairMaterial_->SetShaderParameter("MatDiffColor", hairColor);
    hairMaterial_->SetShaderParameter("MatSpecColor", hairColor * 0.5f);

    Color irisColor{};
    irisColor.FromHSV(Random(), 0.666f, Random(0.5f, 0.9f));
    irisMaterial_->SetShaderParameter("MatDiffColor", irisColor);
    irisMaterial_->SetShaderParameter("MatSpecColor", irisColor);

    for (int e : { 1, 2 }) {
        Color color{ LucKey::RandomColor() };
        Vector3 colorHSV{ color.ToHSV() };
        Color specColor{};
        specColor.FromHSV(LucKey::Cycle(colorHSV.x_ - 0.5f, 0.0f, 1.0f),
                          Min(0.5f, colorHSV.y_), colorHSV.z_ * 0.25f);
        equipment_[e]->GetMaterial()->SetShaderParameter("MatDiffColor", color);
        equipment_[e]->GetMaterial()->SetShaderParameter("MatSpecColor", specColor);
    }

    //Randomize morphs
    for (unsigned m{0}; m < model_->GetNumMorphs(); ++m) {

        StringHash morphNameHash{ model_->GetMorphs().At(m).nameHash_ };

        float newWeight{ Random(-0.5f, 1.0f) };
        if (morphNameHash == StringHash("Lips"))
            newWeight = Clamp(newWeight, 0.42f - skinColor.Luma() * 2.3f, 1.0f);

        model_->SetMorphWeight(m, newWeight);

        //Apply morphs to equipment
        for (AnimatedModel* model : equipment_.Values()) {

            for (const ModelMorph& morph : model->GetMorphs()) {
                if (morph.nameHash_ == morphNameHash) {

                    model->SetMorphWeight(morphNameHash, newWeight);
                }
            }
        }
    }

    animCtrl_->SetTime("Animations/Idle.ani", Random(animCtrl_->GetLength("Animations/Idle.ani")));
}

void Character::PlaySample(Sound* sample)
{
    for (unsigned i{0}; i < sampleSources_.Size(); ++i) {
        
        if (!sampleSources_[i]->IsPlaying()) {

            sampleSources_[i]->Play(sample);
            break;
        }
    }
}

void Character::FixedUpdate(float timeStep)
{
    if (!rigidBody_ || !NETWORK->IsServerRunning())
        return;

    //Movement values
    float thrust{ 450.0f };
//    float maxSpeed{ 18.0f };

    //Apply movement
    Vector3 force{ move_ * thrust * timeStep * (1.0f + 0.42f * actions_[RUN]) };
    rigidBody_->ApplyForce(force);

    //Update animation
    if (rigidBody_->GetLinearVelocity().Length() > 0.1f) {

        animCtrl_->SetStartBone("Animations/Walk.ani", "RootBone");
        animCtrl_->Play("Animations/Walk.ani", 0, true, 0.23f);
        animCtrl_->SetSpeed("Animations/Walk.ani", rigidBody_->GetLinearVelocity().Length() * 1.85f);

        animCtrl_->SetStartBone("Animations/Jab.R.ani", "LowerBack");

    } else {
        animCtrl_->PlayExclusive("Animations/Idle.ani", 0, true, 0.23f);
        //Randomize on first use
        if (animCtrl_->GetTime("Animations/Idle.ani") == 0.0f){
            animCtrl_->SetTime("Animations/Idle.ani", Random(animCtrl_->GetLength("Animations/Idle.ani")));
        }

        animCtrl_->SetStartBone("Animations/Jab.R.ani", "RootBone");
    }

    if (move_.Length())
        AlignWithMovement(timeStep/*, Quaternion(MC->world.camera->GetRotation().EulerAngles().y_, Vector3::UP)*/);
    else if (rigidBody_->GetLinearVelocity().Length() > 0.1f)
        AlignWithVelocity(timeStep);

    if (INPUT->GetKeyPress(KEY_RETURN))
        Randomize();
}

void Character::Update(float timeStep)
{
    Controllable::Update(timeStep);

    if (actionSince_[LAUGH] > actionInterval_[LAUGH])
        Laugh();
}

void Character::EquipRightHand()
{
    rightHand_->SetModel(MC->GetModel("Sword"));
    rightHand_->SetMaterial(MC->GetMaterial("Metal"));
}

void Character::EquipLeftHand()
{
    leftHand_->SetModel(MC->GetModel("Shield"));
    leftHand_->SetMaterial(1, MC->GetMaterial("Leather"));
    leftHand_->SetMaterial(0, MC->GetMaterial("Metal"));
}

void Character::HandleAction(int actionId)
{
    switch(actionId) {
    case HACK:
        if (actionSince_[HACK] > actionInterval_[HACK])
            Hack();
        break;
    case CAST:
        if (actionSince_[CAST] > actionInterval_[CAST])
            Cast();
        break;
    default: break;
    }
}

void Character::Hack()
{
    actionSince_[HACK] = 0.0f;
    animCtrl_->Play("Animations/Jab.R.ani", 1, false, 0.13f);
    animCtrl_->SetSpeed("Animations/Jab.R.ani", 1.8f);
    animCtrl_->SetTime("Animations/Jab.R.ani", 0.0f);
    animCtrl_->SetAutoFade("Animations/Jab.R.ani", 0.1f);

    PlaySample(MC->GetSample("KO/Swing" + String(Random(3) + 1)));
}

void Character::Cast()
{
    actionSince_[CAST] = 0.0f;

    PlaySample(MC->GetSample("KO/Spell1"));
}

void Character::Laugh()
{
    actionSince_[LAUGH] = 0.1f;
    actionInterval_[LAUGH] = Random(5.0f, 23.0f);
    PlaySample(MC->GetSample("KO/Laugh" + String(Random(7) + 1)));

}
