/* KO
// Copyright (C) 2015 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include <Urho3D/Urho3D.h>

#include "luckey.h"

using namespace Urho3D;

class KOCam;
class InputMaster;
class Dungeon;
class Character;
class Player;

typedef struct GameWorld
{
    SharedPtr<KOCam> camera;
    SharedPtr<Node> voidNode;
    SharedPtr<Dungeon> dungeon;
    SharedPtr<Character> ko;
    struct {
        SharedPtr<Node> sceneCursor;
        SharedPtr<Cursor> uiCursor;
        PODVector<RayQueryResult> hitResults;
    } cursor;
} GameWorld;

typedef struct HitInfo
{
    Vector3 position_;
    Vector3 hitNormal_;
    Node* hitNode_;
    Drawable* drawable_;
} HitInfo;

#define MC GetSubsystem<MasterControl>()

class MasterControl : public Application
{
    URHO3D_OBJECT(MasterControl, Application);
    friend class InputMaster;
    friend class KOCam;
public:
    MasterControl(Context* context);
    bool GetAntiAliasing() const noexcept { return antiAliasing_; }
    String GetResourceFolder() const { return resourceFolder_; }

    GameWorld world;

    Scene* scene_;

    Vector< SharedPtr<Player> > players_;
    Vector< SharedPtr<Player> > GetPlayers() const;
    Player* GetPlayer(int playerID) const;

    Material* GetMaterial(String name) const;
    Model* GetModel(String name) const;
    Texture* GetTexture(String name) const;
    Sound* GetMusic(String name) const;
    Sound* GetSample(String name) const;

    virtual void Setup();
    virtual void Start();
    virtual void Stop();
    void Exit();

    float Sine(float freq, float min, float max, float shift = 0.0f);
    void SaveSettings();
private:
    bool antiAliasing_;
    String resourceFolder_;

    SharedPtr<XMLFile> defaultStyle_;

    void CreateConsoleAndDebugHud();

    void CreateScene();
    void CreateUI();

    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    void CreateDungeon(const Vector3 pos);
    void UpdateCursor(double timeStep);
    bool CursorRayCast(double maxDistance, PODVector<RayQueryResult> &hitResults);

    bool paused_;

    Vector<double> sine_;
    void LoadResources();

    void LoadSettings();
};

#endif // MASTERCONTROL_H
