/* KO
// Copyright (C) 2017 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "kocam.h"
#include "character.h"

#include "networkmaster.h"

// UDP port we will use
static const unsigned short SERVER_PORT = 2345;
// Identifier for our custom remote event we use to tell the client which object they control
static const StringHash E_CLIENTOBJECTID("ClientObjectID");
// Identifier for the node ID parameter in the event data
static const StringHash P_ID("ID");

// Control bits we define
static const unsigned CTRL_FORWARD = 1;
static const unsigned CTRL_BACK = 2;
static const unsigned CTRL_LEFT = 4;
static const unsigned CTRL_RIGHT = 8;

NetworkMaster::NetworkMaster(Context* context) : Object(context),
    hidden_{false},
    clientObjectID_{0}
{
    CreateUI();
    SubscribeToEvents();
}

void NetworkMaster::CreateUI()
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    UIElement* root{ GUI->GetRoot() };
    XMLFile* uiStyle{ cache->GetResource<XMLFile>("UI/DefaultStyle.xml") };
    // Set style to the UI root so that elements will inherit it
    root->SetDefaultStyle(uiStyle);

    // Create a Cursor UI element because we want to be able to hide and show it at will. When hidden, the mouse cursor will
    // control the camera, and when visible, it can interact with the login UI
    SharedPtr<Cursor> cursor(new Cursor(context_));
    cursor->SetStyleAuto(uiStyle);
    GUI->SetCursor(cursor);
    // Set starting position of the cursor at the rendering window center
    Graphics* graphics{ GetSubsystem<Graphics>() };
    cursor->SetPosition(graphics->GetWidth() / 2, graphics->GetHeight() / 2);

    // Construct the instructions text element
    instructionsText_ = GUI->GetRoot()->CreateChild<Text>();
    instructionsText_->SetText("Use WASD keys to move");
    instructionsText_->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);
    // Position the text relative to the screen center
    instructionsText_->SetHorizontalAlignment(HA_CENTER);
    instructionsText_->SetVerticalAlignment(VA_CENTER);
    instructionsText_->SetPosition(0, graphics->GetHeight() / 4);
    // Hide until connected
    instructionsText_->SetVisible(false);

    buttonContainer_ = root->CreateChild<UIElement>();
    buttonContainer_->SetFixedSize(500, 20);
    buttonContainer_->SetPosition(20, 20);
    buttonContainer_->SetLayoutMode(LM_HORIZONTAL);

    textEdit_ = buttonContainer_->CreateChild<LineEdit>();
    textEdit_->SetStyleAuto();

    connectButton_ = CreateButton("Connect", 90);
    disconnectButton_ = CreateButton("Disconnect", 100);
    startServerButton_ = CreateButton("Start Server", 110);

    UpdateButtons();
}

Button* NetworkMaster::CreateButton(const String& text, int width)
{
    Font* font{ CACHE->GetResource<Font>("Fonts/Anonymous Pro.ttf") };

    Button* button{ buttonContainer_->CreateChild<Button>() };
    button->SetStyleAuto();
    button->SetFixedWidth(width);

    Text* buttonText{ button->CreateChild<Text>() };
    buttonText->SetFont(font, 12);
    buttonText->SetAlignment(HA_CENTER, VA_CENTER);
    buttonText->SetText(text);

    return button;
}


void NetworkMaster::SubscribeToEvents()
{
    // Subscribe to fixed timestep physics updates for setting or applying controls
    SubscribeToEvent(E_PHYSICSPRESTEP, URHO3D_HANDLER(NetworkMaster, HandlePhysicsPreStep));

    // Subscribe to button actions
    SubscribeToEvent(connectButton_, E_RELEASED, URHO3D_HANDLER(NetworkMaster, HandleConnect));
    SubscribeToEvent(disconnectButton_, E_RELEASED, URHO3D_HANDLER(NetworkMaster, HandleDisconnect));
    SubscribeToEvent(startServerButton_, E_RELEASED, URHO3D_HANDLER(NetworkMaster, HandleStartServer));

    // Subscribe to network events
    SubscribeToEvent(E_SERVERCONNECTED, URHO3D_HANDLER(NetworkMaster, HandleConnectionStatus));
    SubscribeToEvent(E_SERVERDISCONNECTED, URHO3D_HANDLER(NetworkMaster, HandleConnectionStatus));
    SubscribeToEvent(E_CONNECTFAILED, URHO3D_HANDLER(NetworkMaster, HandleConnectionStatus));
    SubscribeToEvent(E_CLIENTCONNECTED, URHO3D_HANDLER(NetworkMaster, HandleClientConnected));
    SubscribeToEvent(E_CLIENTDISCONNECTED, URHO3D_HANDLER(NetworkMaster, HandleClientDisconnected));
    // This is a custom event, sent from the server to the client. It tells the node ID of the object the client should control
    SubscribeToEvent(E_CLIENTOBJECTID, URHO3D_HANDLER(NetworkMaster, HandleClientObjectID));
    // Events sent between client & server (remote events) must be explicitly registered or else they are not allowed to be received
    GetSubsystem<Network>()->RegisterRemoteEvent(E_CLIENTOBJECTID);
}

void NetworkMaster::UpdateButtons()
{
    Connection* serverConnection{ NETWORK->GetServerConnection() };
    bool serverRunning{ NETWORK->IsServerRunning() };

    // Show and hide buttons so that eg. Connect and Disconnect are never shown at the same time
    connectButton_->SetVisible(!serverConnection && !serverRunning);
    disconnectButton_->SetVisible(serverConnection || serverRunning);
    startServerButton_->SetVisible(!serverConnection && !serverRunning);
    textEdit_->SetVisible(!serverConnection && !serverRunning);

    GUI->GetCursor()->SetVisible(true);

    hidden_ = false;
}

void NetworkMaster::HideGUI()
{
    connectButton_->SetVisible(false);
    disconnectButton_->SetVisible(false);
    startServerButton_->SetVisible(false);
    textEdit_->SetVisible(false);

    GUI->GetCursor()->SetVisible(false);

    hidden_ = true;
}

void NetworkMaster::HandlePhysicsPreStep(StringHash eventType, VariantMap& eventData)
{
    // This function is different on the client and server. The client collects controls (WASD controls + yaw angle)
    // and sets them to its server connection object, so that they will be sent to the server automatically at a
    // fixed rate, by default 30 FPS. The server will actually apply the controls (authoritative simulation.)
    Connection* serverConnection{ NETWORK->GetServerConnection() };

    // Client: collect controls
    if (serverConnection) {

        if (!MC->world.camera->GetTarget())
            MC->world.camera->Lock(MC->scene_->GetNode(clientObjectID_));

        Controls controls;

        Vector3 camForward{ LucKey::Scale(MC->world.camera->GetNode()->GetDirection(), Vector3::ONE - Vector3::UP).Normalized() };
        float yaw{ camForward.Angle(Vector3::FORWARD) };

//        if (camForward.Angle(Vector3::LEFT) < 90.0f)
//            yaw = -yaw;

        controls.yaw_ = yaw;

        // Only apply WASD controls if there is no focused UI element
        if (!GUI->GetFocusElement())
        {
            controls.Set(CTRL_FORWARD, INPUT->GetKeyDown(KEY_W));
            controls.Set(CTRL_BACK, INPUT->GetKeyDown(KEY_S));
            controls.Set(CTRL_LEFT, INPUT->GetKeyDown(KEY_A));
            controls.Set(CTRL_RIGHT, INPUT->GetKeyDown(KEY_D));
        }

        serverConnection->SetControls(controls);
        // In case the server wants to do position-based interest management using the NetworkPriority components, we should also
        // tell it our observer (camera) position. In this sample it is not in use, but eg. the NinjaSnowWar game uses it
        serverConnection->SetPosition(MC->world.camera->GetNode()->GetPosition());

    // Server: apply controls to client objects
    } else if (NETWORK->IsServerRunning()) {

        const Vector<SharedPtr<Connection> >& connections{ NETWORK->GetClientConnections() };

        for (unsigned i{0}; i < connections.Size(); ++i) {

            Connection* connection{ connections[i] };
            // Get the object this connection is controlling
            Node* characterNode{ serverObjects_[connection] };
            if (!characterNode)
                continue;

            Character* character{ characterNode->GetComponent<Character>() };

            // Get the last controls sent by the client
            const Controls& controls{ connection->GetControls() };
            // Torque is relative to the forward vector
            Quaternion rotation(0.0f, controls.yaw_, 0.0f);

            Vector3 move{};
            if (controls.buttons_ & CTRL_FORWARD)
                move += Vector3::RIGHT;
            if (controls.buttons_ & CTRL_BACK)
                move += Vector3::LEFT;
            if (controls.buttons_ & CTRL_LEFT)
                move += Vector3::FORWARD;
            if (controls.buttons_ & CTRL_RIGHT)
                move += Vector3::BACK;

            character->SetMove(rotation * move);
        }
    }
}

Node* NetworkMaster::CreateCharacter(Connection* connection)
{
    Node* characterNode{ MC->scene_->CreateChild("Character", REPLICATED) } ;
    if (connection)
        characterNode->SetOwner(connection);

    characterNode->CreateComponent<Character>()->Set(MC->world.dungeon->GetRandomSpawnPoint());

    if (NETWORK->IsServerRunning())
        MC->world.camera->Lock(characterNode);

    return characterNode;
}

void NetworkMaster::HandleConnect(StringHash eventType, VariantMap& eventData)
{
    Network* network{ GetSubsystem<Network>() };
    String address{ textEdit_->GetText().Trimmed() };
    if (address.Empty())
        address = "localhost"; // Use localhost to connect if nothing else specified

    // Connect to server, specify scene to use as a client for replication
    clientObjectID_ = 0; // Reset own object ID from possible previous connection
    network->Connect(address, SERVER_PORT, MC->scene_);

    UpdateButtons();
}

void NetworkMaster::HandleDisconnect(StringHash eventType, VariantMap& eventData)
{
    Network* network{ GetSubsystem<Network>() };
    Connection* serverConnection{ network->GetServerConnection() };
    // If we were connected to server, disconnect. Or if we were running a server, stop it. In both cases clear the
    // scene of all replicated content, but let the local nodes & components (the static world + camera) stay
    if (serverConnection) {

        serverConnection->Disconnect();
        MC->scene_->Clear(true, false);
        clientObjectID_ = 0;

    // Or if we were running a server, stop it
    } else if (network->IsServerRunning()) {

        network->StopServer();
        MC->scene_->Clear(true, false);
    }

    UpdateButtons();
}

void NetworkMaster::HandleStartServer(StringHash eventType, VariantMap& eventData)
{ (void)eventType; (void)eventData;

    Network* network{ GetSubsystem<Network>() };
    network->StartServer(SERVER_PORT);

//    MC->scene_->GetComponent<PhysicsWorld>()->SetInterpolation(false);

    UpdateButtons();
}

void NetworkMaster::HandleConnectionStatus(StringHash eventType, VariantMap& eventData)
{
    UpdateButtons();
}

void NetworkMaster::HandleClientConnected(StringHash eventType, VariantMap& eventData)
{
    using namespace ClientConnected;

    // When a client connects, assign to scene to begin scene replication
    Connection* newConnection{ static_cast<Connection*>(eventData[P_CONNECTION].GetPtr()) };
    newConnection->SetScene(MC->scene_);

    // Then create a controllable object for that client
    Node* newObject{ CreateCharacter(newConnection) };
    serverObjects_[newConnection] = newObject;

    // Finally send the object's node ID using a remote event
    VariantMap remoteEventData;
    remoteEventData[P_ID] = newObject->GetID();
    newConnection->SendRemoteEvent(E_CLIENTOBJECTID, true, remoteEventData);
}

void NetworkMaster::HandleClientDisconnected(StringHash eventType, VariantMap& eventData)
{
    using namespace ClientConnected;

    // When a client disconnects, remove the controlled object
    Connection* connection{ static_cast<Connection*>(eventData[P_CONNECTION].GetPtr()) };
    Node* object{ serverObjects_[connection] };
    if (object)
        object->Remove();

    serverObjects_.Erase(connection);
}

void NetworkMaster::HandleClientObjectID(StringHash eventType, VariantMap& eventData)
{
    clientObjectID_ = eventData[P_ID].GetUInt();
}
