TARGET = ko

LIBS += ../KO/Urho3D/lib/libUrho3D.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    ../KO/Urho3D/include \
    ../KO/Urho3D/include/Urho3D/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    mastercontrol.cpp \
    sceneobject.cpp \
    player.cpp \
    tile.cpp \
    dungeon.cpp \
    kocam.cpp \
    controllable.cpp \
    inputmaster.cpp \
    frop.cpp \
    firepit.cpp \
    deco.cpp \
    wallcollider.cpp \
    floatingeye.cpp \
    npc.cpp \
    luckey.cpp \
    equipment.cpp \
    item.cpp \
    character.cpp \
    networkmaster.cpp

HEADERS += \
    mastercontrol.h \
    sceneobject.h \
    player.h \
    tile.h \
    dungeon.h \
    kocam.h \
    controllable.h \
    inputmaster.h \
    frop.h \
    firepit.h \
    deco.h \
    wallcollider.h \
    floatingeye.h \
    npc.h \
    luckey.h \
    equipment.h \
    item.h \
    character.h \
    networkmaster.h

unix {
    isEmpty(DATADIR) {
        DATADIR = ~/.local/share
    }
    DEFINES += DATADIR=\\\"$${DATADIR}/ko\\\"

    target.path = /usr/games/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/ko/
    resources.files = Resources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = ko.svg
    INSTALLS += icon

    desktop.path = $$DATADIR/applications/
    desktop.files = ko.desktop
    INSTALLS += desktop
}
