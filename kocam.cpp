/* KO
// Copyright (C) 2015 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "character.h"
#include "kocam.h"

using namespace LucKey;

void KOCam::RegisterObject(Context *context)
{
    context->RegisterFactory<KOCam>();
}

KOCam::KOCam(Context *context) : LogicComponent(context),
    camera_{},
    target_{},
    smoothTargetPosition_{Vector3::ZERO},
    smoothTargetVelocity_{Vector3::ZERO},
    yaw_{0.0f},
    pitch_{60.0f},
    velocity_{Vector3::ZERO},
    maxVelocity_{23.0f},
    acceleration_{7.0f},
    rotationSpeed_{0.0f},
    maxRotationSpeed_{512.0f},
    angularAcceleration_{128.0f},
    velocityMultiplier_{1.0f}
{
}

void KOCam::OnNodeSet(Node *node)
{ if (!node) return;

    AUDIO->SetListener(node_->CreateComponent<SoundListener>());

    float viewRange{ 128.0f };

    //Create the camera. Limit far clip distance to match the fog
    node_->SetPosition(Vector3::UP * 7.0f);//MC->world.ko->GetPosition() + Vector3(0.23f, 5.0f, -4.2f));
    node_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
    camera_ = node_->CreateComponent<Camera>();
    camera_->SetFarClip(viewRange);
    camera_->SetNearClip(0.023f);

    zone_ = node_->CreateComponent<Zone>();
    zone_->SetBoundingBox(BoundingBox(Vector3(-100.0f, -50.0f, -100.0f), Vector3(100.0f, 50.0f, 100.0f)));
    zone_->SetFogColor(Color(0.0f, 0.0f, 0.0f, 1.0f));
    zone_->SetFogStart(10.0f);
    zone_->SetFogEnd(viewRange - 5.0f);

    SetupViewport();
}

void KOCam::SetupViewport()
{
    //Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
    SharedPtr<Viewport> viewport(new Viewport(context_, GetScene(), camera_));
    viewport_ = viewport;

    //Use deferred render path
//        XMLFile* file{ new XMLFile(context_) };
//        file->LoadFile("RenderPaths/Deferred.xml");
//        viewport_->SetRenderPath(file);
    //Add anti-asliasing and HDR bloom

    effectRenderPath_ = viewport_->GetRenderPath();
    effectRenderPath_->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath_->SetEnabled("FXAA3", MC->GetAntiAliasing());
    effectRenderPath_->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath_->SetShaderParameter("BloomHDRThreshold", 0.42f);
    effectRenderPath_->SetShaderParameter("BloomHDRMix", Vector2(0.75f, 0.5f));
    effectRenderPath_->SetEnabled("BloomHDR", true);

    viewport_->SetRenderPath(effectRenderPath_);
    RENDERER->SetViewport(0, viewport);
}

Vector3 KOCam::GetWorldPosition() const
{
    return node_->GetWorldPosition();
}

Quaternion KOCam::GetRotation() const
{
    return node_->GetRotation();
}

void KOCam::PostUpdate(float timeStep)
{
    if (!camera_)
        return;

    Vector3 targetPosition{ target_ ? target_->GetWorldPosition() : Vector3::ZERO};
    RigidBody* targetBody{ target_ ? target_->GetComponent<RigidBody>() : nullptr };
    Vector3 targetVelocity{ targetBody ? targetBody->GetLinearVelocity() : Vector3::ZERO };

    Input* input{ GetSubsystem<Input>() };

    //Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
    Vector3 camForward{ LucKey::Scale(node_->GetDirection(), Vector3::ONE - Vector3::UP).Normalized() };

    Vector3 normalizedPlanarDirection{ LucKey::Scale( node_->GetDirection(), Vector3::ONE - Vector3::UP ).Normalized() };
    if (input->GetKeyDown(KEY_T)) velocity_ +=  normalizedPlanarDirection * acceleration_ * timeStep;
    if (input->GetKeyDown(KEY_G)) velocity_ += -normalizedPlanarDirection * acceleration_ * timeStep;
    if (input->GetKeyDown(KEY_H)) rotationSpeed_ -= angularAcceleration_ * timeStep;
    if (input->GetKeyDown(KEY_F)) rotationSpeed_ += angularAcceleration_ * timeStep;
    if (input->GetKeyDown(KEY_Y)) velocity_ += Vector3::UP   * acceleration_ * timeStep;
    if (input->GetKeyDown(KEY_R)) velocity_ += Vector3::DOWN * acceleration_ * timeStep;

    //Mouse move
    if (input->GetMouseButtonDown(MOUSEB_MIDDLE))
        rotationSpeed_ += input->GetMouseMoveX();

    velocity_ += 0.23f * Vector3::DOWN * acceleration_ * input->GetMouseMoveWheel();

    //Read joystick input
    JoystickState* joystickState{ input->GetJoystickByIndex(0) };
    if (joystickState){
        rotationSpeed_ -= joystickState->GetAxisPosition(2) * timeStep * angularAcceleration_;
        velocity_ -= joystickState->GetAxisPosition(3) * camForward * acceleration_ * timeStep;
//        if (joystickState->GetButtonDown(JB_R2)) velocity_ += Vector3::UP;
//        if (joystickState->GetButtonDown(JB_L2) && rootNode_->GetPosition().y_ > 1.0f) velocity_ += Vector3::DOWN;
    }

    velocity_ *= 0.95f;
    rotationSpeed_ *= 0.95f;


    if ( velocityMultiplier_ < 8.0f && (input->GetKeyDown(KEY_LSHIFT) || input->GetKeyDown(KEY_RSHIFT)) ) {

        velocityMultiplier_ += 0.23f;

    } else {

        velocityMultiplier_ = pow(velocityMultiplier_, 0.75f);
    }
    node_->Translate(velocity_ * velocityMultiplier_ * timeStep, TS_WORLD);

    //Rotate left and right
    Clamp(rotationSpeed_, -maxRotationSpeed_, maxRotationSpeed_);
    node_->RotateAround(targetPosition, Quaternion(rotationSpeed_ * velocityMultiplier_ * timeStep, Vector3::UP), TS_WORLD);

    //Prevent camera from going too low
    float yPos{ node_->GetPosition().y_ };
    if (yPos < Y_MIN) {
        velocity_.y_ = velocity_.y_ < 0.0f ? 0.0f : velocity_.y_;
        node_->SetPosition(Vector3(node_->GetPosition().x_, (yPos - Y_MIN) * timeStep + Y_MIN, node_->GetPosition().z_));
    } else if (yPos > Y_MAX) {
        velocity_.y_ = velocity_.y_ > 0.0f ? 0.0f : velocity_.y_;
        node_->SetPosition(Vector3(node_->GetPosition().x_, (yPos - Y_MAX) * timeStep + Y_MAX, node_->GetPosition().z_));
    }

    smoothTargetPosition_ = Lerp(smoothTargetPosition_, targetPosition + smoothTargetVelocity_ * (1.0f + node_->GetPosition().y_ * 0.1f), Clamp(4.2f * timeStep, 0.0f, 1.0f));
    smoothTargetVelocity_ = Lerp(smoothTargetVelocity_, targetVelocity, Clamp(timeStep, 0.0f, 1.0f));
    node_->Translate(smoothTargetVelocity_ * timeStep, TS_WORLD);
    Quaternion camRot{ node_->GetWorldRotation() };
    Quaternion aimRotation{ camRot };
    aimRotation.FromLookRotation(smoothTargetPosition_ - node_->GetWorldPosition());
    node_->SetRotation(camRot.Slerp(aimRotation, Clamp(5.0f * timeStep, 0.0f, 1.0f)));

    //Update camera and zone
    float targetDistance{ LucKey::Distance(node_->GetWorldPosition(), targetPosition) };
    float viewDistance{ 2.666f * targetDistance };
    camera_->SetFov(Max(23.0f, 90.0f - targetDistance));
    camera_->SetFarClip(viewDistance);
    zone_->SetFogStart(1.13f * targetDistance);
    zone_->SetFogEnd(viewDistance);
}

void KOCam::Lock(Node* target)
{
    target_ = target;

    if (target_)
        node_->SetPosition(target_->GetWorldPosition() - node_->GetDirection() * node_->GetDirection().ProjectOntoAxis(Vector3::DOWN) * node_->GetWorldPosition().y_);
}
