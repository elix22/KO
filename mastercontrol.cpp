/* KO
// Copyright (C) 2015 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "networkmaster.h"
#include "inputmaster.h"
#include "kocam.h"
#include "dungeon.h"
#include "player.h"
#include "character.h"
#include "floatingeye.h"
#include "firepit.h"
#include "frop.h"

#include "mastercontrol.h"

URHO3D_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context *context):
    Application(context),
    antiAliasing_{true},
    paused_(false),
    world{}
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());
    NETWORK->SetUpdateFps(60);

    engineParameters_[EP_WINDOW_TITLE] = "KO and the Curse of Greyface";
    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("urho3d", "logs")+"KO.log";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;

    //Add resource paths
    String resourcePaths{};

    /*if (FILES->DirExists(FILES->GetAppPreferencesDir("luckey", "ko")))
        resourcePaths = FILES->GetAppPreferencesDir("luckey", "ko");
    else*/ if (FILES->DirExists("Resources"))
        resourcePaths = "Resources";
    else if (FILES->DirExists("../KO/Resources"))
        resourcePaths = "../KO/Resources";

    resourceFolder_ = resourcePaths;
    resourcePaths += ";";

    if (FILES->DirExists("Data"))
        resourcePaths += "Data;";
    if (FILES->DirExists("CoreData"))
        resourcePaths += "CoreData;";

    engineParameters_[EP_RESOURCE_PATHS] = resourcePaths;

    LoadSettings();
}
void MasterControl::LoadSettings()
{
    if (FILES->FileExists("Resources/Settings.xml")){
        File file(context_, "Resources/Settings.xml", FILE_READ);
        XMLFile configFile(context_);
        configFile.Load(file);
        XMLElement graphics{ configFile.GetRoot().GetChild("Graphics") };
        XMLElement audio{ configFile.GetRoot().GetChild("Audio") };

        if (graphics) {

            engineParameters_[EP_WINDOW_WIDTH] = graphics.GetInt("Width");
            engineParameters_[EP_WINDOW_HEIGHT] = graphics.GetInt("Height");
            engineParameters_[EP_FULL_SCREEN] = graphics.GetBool("Fullscreen");

            antiAliasing_ = graphics.GetBool("AntiAliasing");
        }
        if (audio) {

            AUDIO->SetMasterGain(SOUND_MUSIC, audio.GetFloat("MusicGain"));
        }
    }
}
void MasterControl::SaveSettings()
{
    XMLFile file(context_);
    XMLElement root(file.CreateRoot("Settings"));

    XMLElement graphicsElement(root.CreateChild("Graphics"));
    graphicsElement.SetInt("Width", GRAPHICS->GetWidth());
    graphicsElement.SetInt("Height", GRAPHICS->GetHeight());
    graphicsElement.SetBool("Fullscreen", GRAPHICS->GetFullscreen());
    graphicsElement.SetBool("AntiAliasing", antiAliasing_);

    XMLElement audioElement(root.CreateChild("Audio"));
    audioElement.SetFloat("MusicGain", AUDIO->GetMasterGain(SOUND_MUSIC));

    file.SaveFile("Resources/Settings.xml");
}

void MasterControl::Start()
{
    FloatingEye::RegisterObject(context_);
    FirePit::RegisterObject(context_);
    Frop::RegisterObject(context_);
    KOCam::RegisterObject(context_);
    Character::RegisterObject(context_);

    context_->RegisterSubsystem(this);
    context_->RegisterSubsystem(new InputMaster(context_));

    LoadResources();

    CreateConsoleAndDebugHud();
    CreateScene();
    CreateUI();

    context_->RegisterSubsystem(new NetworkMaster(context_));

//    Sound* music{ CACHE->GetResource<Sound>("Music/Pantera_Negra_-_Sumerian_Speech.ogg") };
    Sound* music{ CACHE->GetResource<Sound>("Music/Modanung_-_CastIron_Corridor.ogg") };
    music->SetLooped(true);
    Node* musicNode{ scene_->CreateChild("Music") };
    SoundSource* musicSource{ musicNode->CreateComponent<SoundSource>() };
    musicSource->SetSoundType(SOUND_MUSIC);
    musicSource->Play(music);

//    musicSource->SetFrequency(55000);


    SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(MasterControl, HandlePostRenderUpdate));
}
void MasterControl::Stop()
{
    SaveSettings();

    engine_->DumpResources(true);
}

void MasterControl::CreateConsoleAndDebugHud()
{
    // Create console
    Console* console{ engine_->CreateConsole() };
    console->SetDefaultStyle(defaultStyle_);
    console->GetBackground()->SetOpacity(0.8f);

    // Create debug HUD.
    DebugHud* debugHud{ engine_->CreateDebugHud() };
    debugHud->SetDefaultStyle(defaultStyle_);

//    debugHud->SetMode(DEBUGHUD_SHOW_ALL);
}

void MasterControl::CreateUI()
{
   /* ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    UI* ui{ GetSubsystem<UI>() };

    //Create a Cursor UI element because we want to be able to hide and show it at will. When hidden, the mouse cursor will control the camera
    world.cursor.uiCursor = new Cursor(context_);
    world.cursor.uiCursor->SetVisible(false);
    ui->SetCursor(world.cursor.uiCursor);

    //Set starting position of the cursor at the rendering window center
    world.cursor.uiCursor->SetPosition(graphics_->GetWidth() / 2, graphics_->GetHeight() / 2);*/

    //Construct new Text object, set string to display and font to use
    Text* instructionText{ GUI->GetRoot()->CreateChild<Text>() };
    instructionText->SetText("KO");
    instructionText->SetFont(CACHE->GetResource<Font>("Fonts/ChaosTimes_lig.ttf"), 32.0f);
    //The text has multiple rows. Center them in relation to each other
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_CENTER);
    instructionText->SetPosition(0, static_cast<int>(GUI->GetRoot()->GetHeight() / 2.1f));
    instructionText->SetColor(Color(1.0f, 0.023f, 0.0f, 0.5f));
}

void MasterControl::LoadResources()
{
    // Get default style
    defaultStyle_ = CACHE->GetResource<XMLFile>("UI/DefaultStyle.xml");

}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);

    //Create octree, use default volume (-1000, -1000, -1000) to (1000,1000,1000)
    /*Octree* octree = */scene_->CreateComponent<Octree>(LOCAL);
    //octree->SetSize(BoundingBox(Vector3(-10000, -100, -10000), Vector3(10000, 1000, 10000)), 1024);
    /*PhysicsWorld* physicsWorld = */scene_->CreateComponent<PhysicsWorld>(LOCAL);
//    scene_->CreateComponent<DebugRenderer>();

    //Create cursor
    world.cursor.sceneCursor = scene_->CreateChild("Cursor", LOCAL);
    world.cursor.sceneCursor->SetPosition(Vector3(0.0f,0.0f,0.0f));
    /*StaticModel* cursorObject = */world.cursor.sceneCursor->CreateComponent<StaticModel>();
    //cursorObject->SetModel(cache_->GetResource<Model>("Models/Cursor.mdl"));
    //cursorObject->SetMaterial(cache_->GetResource<Material>("Materials/glow.xml"));

    //Create an invisible plane for mouse raycasting
    world.voidNode = scene_->CreateChild("Void", LOCAL);
    //Location is set in update since the plane moves with the camera.
    world.voidNode->SetScale(Vector3(1000.0f, 1.0f, 1000.0f));
    StaticModel* planeObject{ world.voidNode->CreateComponent<StaticModel>() };
    planeObject->SetModel(GetModel("Plane"));
    planeObject->SetMaterial(GetMaterial("Invisible"));

    //Create a directional light to the world. Enable cascaded shadows on it
    Node* lightNode{ scene_->CreateChild("DirectionalLight", LOCAL) };
    lightNode->SetDirection(Vector3(0.0f, -1.0f, 0.0f));
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetBrightness(0.23f);
    light->SetColor(Color(1.0f, 0.8f, 0.7f));
    light->SetCastShadows(true);
    light->SetShadowIntensity(0.5f);
    light->SetSpecularIntensity(0.0f);
    light->SetShadowBias(BiasParameters(0.00025f, 0.5f));
    light->SetShadowResolution(0.23f);
    //Set cascade splits at 10, 50, 200 world unitys, fade shadows at 80% of maximum shadow distance
    light->SetShadowCascade(CascadeParameters(7.0f, 23.0f, 42.0f, 500.0f, 0.8f));

//    world.ko = scene_->CreateChild()
//            ->CreateComponent<Character>();

//    players_.Push(SharedPtr<Player>(new Player(1, context_)));
//    INPUTMASTER->SetPlayerControl(GetPlayer(1), world.ko);

    world.dungeon = new Dungeon(context_);

    Node* camNode{ scene_->CreateChild("Camera", LOCAL) };
    world.camera = camNode->CreateComponent<KOCam>(LOCAL);
//    world.camera->Lock(world.ko->GetNode());
}

void MasterControl::Exit()
{
    engine_->Exit();
}

float MasterControl::Sine(float freq, float min, float max, float shift)
{
    float phase{ freq * scene_->GetElapsedTime() + shift };
    float add{ 0.5f * (min + max) };
    return LucKey::Sine(phase) * 0.5f * (max - min) + add;
}

void MasterControl::HandlePostRenderUpdate(StringHash eventType, VariantMap &eventData)
{
//    if (AUDIO->GetMasterGain(SOUND_MUSIC) != 1.0f)
//        AUDIO->SetMasterGain(SOUND_MUSIC, Pow(Clamp(0.42f * scene_->GetElapsedTime() - 0.23f, 0.0f, 1.0f), 2.0f));

    scene_->GetComponent<PhysicsWorld>()->DrawDebugGeometry(true);
}

Vector<SharedPtr<Player> > MasterControl::GetPlayers() const {
    return players_;
}

Player* MasterControl::GetPlayer(int playerID) const
{
    for (Player* p : players_) {

        if (p->GetPlayerId() == playerID)
            return p;
    }

    return nullptr;
}

Material* MasterControl::GetMaterial(String name) const
{
    return CACHE->GetResource<Material>("Materials/" + name + ".xml");
}
Model* MasterControl::GetModel(String name) const
{
    return CACHE->GetResource<Model>("Models/" + name + ".mdl");
}

Texture* MasterControl::GetTexture(String name) const
{
    return CACHE->GetResource<Texture>("Textures/" + name + ".png");
}

Sound* MasterControl::GetSample(String name) const
{
    return CACHE->GetResource<Sound>("Samples/" + name + ".ogg");
}
