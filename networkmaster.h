/* HoverAce
// Copyright (C) 2017 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/



#ifndef GUIMASTER_H
#define GUIMASTER_H

#include <Urho3D/Urho3D.h>
#include "mastercontrol.h"

class NetworkMaster : public Object
{
    URHO3D_OBJECT(NetworkMaster, Object);
public:
    NetworkMaster(Context* context);
    void UpdateButtons();
    void HideGUI();

    bool IsHidden() const { return hidden_; }
private:
    bool hidden_;

    // Mapping from client connections to controllable objects.
    HashMap<Connection*, WeakPtr<Node> > serverObjects_;
    // ID of own controllable object (client only.)
    unsigned clientObjectID_;

    SharedPtr<UIElement> buttonContainer_;
    SharedPtr<LineEdit> textEdit_;
    SharedPtr<Button> connectButton_;
    SharedPtr<Button> disconnectButton_;
    SharedPtr<Button> startServerButton_;
    SharedPtr<Text> instructionsText_;

    void CreateUI();
    Button* CreateButton(const String& text, int width);
    Node* CreateCharacter(Connection* connection);

    void SubscribeToEvents();

    void HandlePhysicsPreStep(StringHash eventType, VariantMap& eventData);
    void HandleConnect(StringHash eventType, VariantMap& eventData);
    void HandleDisconnect(StringHash eventType, VariantMap& eventData);
    void HandleStartServer(StringHash eventType, VariantMap& eventData);
    void HandleConnectionStatus(StringHash eventType, VariantMap& eventData);
    void HandleClientConnected(StringHash eventType, VariantMap& eventData);
    void HandleClientDisconnected(StringHash eventType, VariantMap& eventData);
    void HandleClientObjectID(StringHash eventType, VariantMap& eventData);
};

#endif // GUIMASTER_H
